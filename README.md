calc.pl
=======

'papertape' calculator running in terminal. Perl

For help enter  ```help()```


Predefined functions:
--------------------

```helpf()```

available funcions:  sqrt exp log ln atan asin acos sin cos tan p0 p1 p2 p3 p4 pow getinter solveinterm solveinteri bb2t12 bw2t12 t122bb reratet rerate fluxap fluxpa rho t2gcm gcm2t react printreact kinem mex amu b2b rad deg xmgraceon xmgraceoff tof2e e2tof time2sec sec2timed sec2time mat helpcatch helpm helpv helpfyz helpf help quit avg median sum lr readf readcsvf batch fillf writef printf latexf showf sortrevf sortf drawf drawof


Predefined variables:
----------------------

```helpv()```
predefined PHYSICAL CONSTANTS
    e = 2.71828182845904523536  # euler constant
   pi = 3.141592653589          # PI
 hbar = 6.581E-22               # MeV.s   planck constant/2PI
hplanck   #  planck's h  in m2kg/s
   ec = 1.602E-19               # Coulomb - electron charge
hbarc = 197                     # MeV.fm
    k = 8.617343E-11      # Boltzman MeV/Kelvin 
  kjoul=1.38064e-23  # Boltzman k  in [m2kgs-2K-1]=[J/K-1]
 kevolt=8.6173324e-5 # Boltzman k  in [eV K-1]
  massp=938.2720     # [MeV]
  massn=939.5654     # [MeV]


Physics functions:
-------------------

```helpfyz()```

PHYS    | ------------------------------------------------------------
  conv B| Time <=> red.tr.prob. 
        |     bb2t12(keV, B[eb],'sigL')   ... eb   -> partial level lifetime
        |     bw2t12(keV, B[wu],'sigL',A) ... w.u. -> partial level lifetime
        |     t122bb(keV, T12,  'sigL') ...  partial level lifetime -> eb 
        | Reduced.transition.probabilities. in different units:
        |     b2b( Bvalue,  sigmaL, conversion, A): 
        |       eg. b2b(101,'E2','FW',77)
        |       conv: BF BW FB FW WB WF  (uppercase to avoid collision)
        |       A if not supplied, taken from var a. e.g.a=32 (just W.U.)
        |     btw:
        |      B(E2,0->2,A=46)=196e2fm4=20wu.  B(E2 2->0)=1/5(0->2)=4wu
        |      B(E2,0->2,A=44)=0.0314e2b2=34wu.B(E2 2->0)=1/5(0->2)=6.8wu
        |      delta=sqrt(   bw2t12(ene,bm1,'M1',a)/ ( bw2t12(ene,be2,'E2',a) )  )
        |
        | Neutron tof <=> Energy_n:
        |     tof2e(tof,distance,A), e2tof(E,dist,A)..
        |     neutron tof in ns, dist in meters, A-mother nucleus
        |
        | Reactions:
        |    rerate(flux, sigma, MM, thickness)=reaction rate
        |          flux (particles/s), cross section (barn), 
        |          target: MolarM (usually mass number), thickness(g/cm2)
        |    reratet(flux,sigma, MM, tgt thickness in um, rho_in_g_cm3)
        |    fluxap( beamint (Amp), charge ) Intensity in Amps=> part/s
        |    fluxpa( beamint (part/s), charge )  p/s => Ampers
        |       e.g. fluxap(2e-6, 22)*7*(24:00:00) = #ions/week at 2uA
        |    t2gcm( t, density ) thickness (in cm) => g/cm2
        |    gcm2t( t in g/cm2, density ) => thickness (in cm) 
        |    rho('Al') = density of Al
        |       e.g. t2gcm(0.1, rho('al') )=0.2698 : 1mm Al=0.27g/cm2

Reactions, kinematics:
------------------------

```    react(2,1, 14,6, 1,1 , 15,6,  T1,  angle3) ``` ... 14C(d,p)15C

  (A,Z proj, tgt, eject, remn,  TKE,  det.angle)
        |        (A,Z:proj,tgt,eject,remn,TKE,ang, Eexc,amu1,amu2,amu3,amu4)
        |    printreact(9,4,12,6) ... prints many reactions for 9Be+12C

        | Masses:
        |    mex(14,6) = mass excess 14C

        |    amu(2,1)  =  atomic mass of deuteron ( 1amu = 931.49403 )

        |    mex(4,2)+mex(4,2) - mex(8,4)  =  Q reaction 2alpha->8Be (<0)
        |    mex(16,8)+mex(2,1)- mex(18,9) = 7.525 Q react. 16O+d=>18F

	Separation energies
```        |    mex(12,6)+mex(1,0)-mex(13,6)```  =  S1n in  13C
```        |    mex(8,3)+mex(1,1)-mex(9,4) ```   =  S1p in  9Be


